# Security tools set up in a docker-compose environment

This repo contains a set of various docker based security tools as well as some instructions on how to use them.

Each service has some comments in the docker-compose.yml, or below.

# Tools

## Docker-compose

See the docker-compose file for all the microservice tools

## Anchore

The Anchore Engine allows developers to perform detailed analysis on their container images, run queries, produce reports and define policies that can be used in CI/CD pipelines. Developers can extend the tool to add new plugins that add new queries, new image analysis, and new policies.

Basically auditing containers

See more details in the achore/README

### Usage case
Run on deployed docker images both custom made and tools we use in this repository to stay informed about vulnerabilities.


## OSSEC

Open source host based IDS

### Usage case

Could be used to deploy lightweight and easily managable host-based IDS on linux machines without using up licenses on any other software.  It could also be used in addition to any host-based IDS we currently use.


## Seccubus

Tool to automate scans and host management for openvas.

### Use case
While this tools seems like it could be useful, the platform is old and seems difficult to understand and configure.  If we invested some time into it, we could automate scanning and host management with openvas for more of a "hands-off" vulnerability management on smaller networks (for simplicity)


## ZAP

Zed attack proxy.  This is a tool for running automated tests against a website.  The tool is still actively maintained but is written in java...
However it is possible to run it in a web browser using the docker image in `docker-compose.yaml`.  It runs alright, but is a bit slow and clunky.

### Use case

Despite the clunky-ness of the app, it is useful and can be used as a basic gatekeeper to make sure no web apps are deployed with clear vulnerabilites or lack of best practices.

TODO: Look into using zap automated tests in CICD.

## docker-bench-security

## Cilium

## Suricata

IDS similar to snort.

## EveBox

Web frontend to manage events generateed by Suricata

## Scirius

Web frontend to manage events and ruleset from Suricata.


## Chrome

Chrome headless for simulating chrome in a docker container and interacting over port 9222