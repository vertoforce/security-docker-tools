# Info

The Anchore Engine allows developers to perform detailed analysis on their container images, run queries, produce reports and define policies that can be used in CI/CD pipelines. Developers can extend the tool to add new plugins that add new queries, new image analysis, and new policies.

Basically auditing containers

# Setup

https://anchore.freshdesk.com/support/solutions/articles/36000020729-install-with-docker-compose

```
docker-compose up
# Check feed sync progress:
docker-compose exec anchore-engine anchore-cli --u admin --p foobar system feeds list
# Submit an image:
docker-compose exec anchore-engine anchore-cli --u admin --p foobar image get docker.io/library/debian:7 | grep 'Analysis Status'
# Check vulnerabilities
docker-compose exec anchore-engine anchore-cli --u admin --p foobar image vuln docker.io/library/debian:7 all
```